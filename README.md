# ncdcinstaller

![](https://img.shields.io/badge/written%20in-Java%20%28Android%29-blue)

ncdc for Android.

Yorhel's official statically linked binary works fine on Android, but it's cumbersome to find a free `rwx` mount to run it from, and configure persistent settings storage. This one-click app solves that small annoyance.

- Requires "Android Terminal Emulator" from the market (or bundled with your rom)
- Does not require root.
- Requires Android 2.3 or newer (API 9, for `File.setExecutable`).
- The official `ncdc` 1.18.1 binary is included under the terms of the MIT license.

Yorhel's `ncdc` homepage: http://dev.yorhel.nl/ncdc

Tags: nmdc

## Known Bugs

Android puts the zoneinfo data somewhere unexpected, so ncdc may be forced into UTC.

Android's DNS stack doesn't play nice with ncdc.
To fix, run the following commands as root to build a "normal-looking" DNS infrastructure;


```
mount -o remount, rw /system
echo nameserver 8.8.8.8 > /system/etc/resolv.conf
echo nameserver 8.8.4.4 >> /system/etc/resolv.conf
chmod 644 /system/etc/resolv.conf
mount -o remount, ro /system
```


The above few bugs would be fixable with a chroot, but that requires root access, and if you have that this app is unnecessary. They would also be fixable by recompiling ncdc against android's libc.

## Changelog

1.18.1~android2
- Useful version number
- Set `TMPDIR` environment variable to fix sqlite errors
- Automatically open ncdc

1.0
- Initial release


## Download

- [⬇️ NCDCInstaller-1.18.1~android2__source.7z](dist-archive/NCDCInstaller-1.18.1~android2__source.7z) *(1.87 MiB)*
- [⬇️ NCDCInstaller-1.18.1~android2.apk](dist-archive/NCDCInstaller-1.18.1~android2.apk) *(2.02 MiB)*
- [⬇️ NCDCInstaller-1.0__source.7z](dist-archive/NCDCInstaller-1.0__source.7z) *(1.87 MiB)*
- [⬇️ NCDCInstaller-1.0.apk](dist-archive/NCDCInstaller-1.0.apk) *(2.02 MiB)*
